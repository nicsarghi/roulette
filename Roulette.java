

import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);   
        RouletteWheel wheel = new RouletteWheel();
        boolean playing;
        System.out.println("How much would you like to bet?");
        int money = s.nextInt(); 
        int timesWon = 0;
        int timesLost = 0;

        do{// run the game
            BetResult betResult = game(s,wheel);
            money+=betResult.profit;
            if(betResult.win)
                timesWon++;
            else
                timesLost++;    
            // ask if the player wants to quit each round
            if (money > 0){
                playing = askToQuit(s);
            }
            else{// unless they run out of money, then it autoquits
                System.out.println("No more money to bet. Quiting...");
                playing = false;
            } 
        }while(playing);
        System.out.println("Your balance: "+money);
        System.out.println("Times won: "+timesWon+" | Times lost: "+timesLost);

    }
    public static BetResult game(Scanner s, RouletteWheel wheel){
        // spin wheel and print result
        wheel.spin();
        int landedNumber = wheel.getValue();
        System.out.println(landedNumber);

        // ask user about bet
        System.out.println("Input bet amount");
        int bet = s.nextInt();
        System.out.println("Input the number you're betting on");
        int betNumber = s.nextInt();
       
        System.out.println("Landed on "+landedNumber);
        // determine if the user won
        boolean betWon = landedNumber == betNumber;
        int profit = betWon? bet*35 : -bet;
        BetResult result = new BetResult(profit,betWon);
        // print result
        if(betWon)
            System.out.println("You won "+profit+"$!");
        else
            System.out.println("You lost "+(-profit)+"$");

        return result; 
    }
    public static boolean askToQuit(Scanner s){
        /// ask to play again 
        System.out.println("Play again?");
        String response = s.next().toLowerCase();
        // if the answer starts with 'y', (yes) then the user keeps playing
        boolean answer = response.charAt(0)=='y' ? true : false;
        return answer;  
    } 
}
