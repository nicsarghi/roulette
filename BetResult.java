public class BetResult {
    public int profit;
    public boolean win; 
    public BetResult(int profit, boolean win){
        this.profit = profit;
        this.win = win;
    }
}
